from typing import Callable, Optional


def zero(operator: Optional[Callable] = None) -> int:
    """Returns number zero

    Returns:
        int: [returns 0]
    """
    if not operator:
        return 0
    return operator(zero)


def one(operator: Optional[Callable] = None) -> int:
    """Returns number once

    Returns:
        int: [returns 1]
    """
    if not operator:
        return 1
    return operator(one)


def two(operator: Optional[Callable] = None) -> int:
    """Returns number two

    Returns:
        int: [returns 2]
    """
    if not operator:
        return 2
    return operator(two)


def three(operator: Optional[Callable] = None) -> int:
    """Returns number three

    Returns:
        int: [returns 3]
    """
    if not operator:
        return 3
    return operator(three)


def four(operator: Optional[Callable] = None) -> int:
    """Returns number four

    Returns:
        int: [returns 4]
    """
    if not operator:
        return 4
    return operator(four)


def five(operator: Optional[Callable] = None) -> int:
    """Returns number five

    Returns:
        int: [returns 5]
    """
    if not operator:
        return 5
    return operator(five)


def six(operator: Optional[Callable] = None) -> int:
    """Returns number six

    Returns:
        int: [returns 6]
    """
    if not operator:
        return 6
    return operator(six)


def seven(operator: Optional[Callable] = None) -> int:
    """Returns number seven

    Returns:
        int: [returns 7]
    """
    if not operator:
        return 7
    return operator(seven)


def eight(operator: Optional[Callable] = None) -> int:
    """Returns number eight

    Returns:
        int: [returns 8]
    """
    if not operator:
        return 8
    return operator(eight)


def nine(operator: Optional[Callable] = None) -> int:
    """Returns number nine

    Returns:
        int: [returns 9]
    """
    if not operator:
        return 9
    return operator(nine)
