from typing import Callable


def plus(x: Callable = 0):
    def do_operation(y: Callable = 0):
        return x() + y()
    return do_operation


def minus(x: Callable):
    def do_operation(y: Callable = 0):
        return y() - x()
    return do_operation


def times(x: Callable):
    def do_operation(y: Callable = 0):
        return x() * y()
    return do_operation


def divided_by(x: Callable):
    def do_operation(y: Callable = 0):
        return y() / x()
    return do_operation
