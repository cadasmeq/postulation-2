import pytest

from ex1 import numbers


@pytest.mark.parametrize('number_function, expected_value', [
    (numbers.zero, 0),
    (numbers.one, 1),
    (numbers.two, 2),
    (numbers.three, 3),
    (numbers.four, 4),
    (numbers.five, 5),
    (numbers.six, 6),
    (numbers.seven, 7),
    (numbers.eight, 8),
    (numbers.nine, 9),
])
def test_validate_numbers(number_function, expected_value):
    """
    Given an input function which its output correspond to an specific number,
    this test will validate whether or not its output is an Integer type and
    output number is what is expected.

    Args:
        number_function ([Callable]): [Function that returns a number from 0-9]
        expected_value ([int]): [Expected number returned from number_function]
    """
    assert isinstance(number_function(), int)
    assert number_function() == expected_value
