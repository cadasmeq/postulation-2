import pytest

from ex1 import numbers
from ex1.operators import (
    plus,
    minus,
    divided_by,
    times,
)


one = numbers.one
two = numbers.two
three = numbers.three
four = numbers.four
five = numbers.five
six = numbers.six
seven = numbers.seven
eight = numbers.eight
nine = numbers.nine
zero = numbers.zero


@pytest.mark.parametrize('x,y,result', [
    (one, four, 5),
    (two, four, 6),
    (zero, four, 4),
    (seven, two, 9),
    (seven, one, 8),
])
def test_plus(x, y, result):
    """Test that plus two numbers"""
    assert x(plus(y)) == result


@pytest.mark.parametrize('x,y,result', [
    (four, one, 3),
    (seven, two, 5),
    (eight, one, 7),
    (nine, seven, 2),
])
def test_minus(x, y, result):
    """Test that minus two numbers"""
    assert x(minus(y)) == result


@pytest.mark.parametrize('x,y,result', [
    (four, one, 4),
    (seven, two, 14),
    (eight, one, 8),
    (nine, seven, 63),
])
def test_times(x, y, result):
    """Test that multiplies two numbers"""
    assert x(times(y)) == result


@pytest.mark.parametrize('x,y,result', [
    (four, one, 4),
    (eight, two, 4),
    (nine, three, 3),
    (eight, four, 2),
])
def test_division(x, y, result):
    """Test that divides two numbers"""
    assert x(divided_by(y)) == result


@pytest.mark.xfail
@pytest.mark.parametrize('x,y', [
    (four, zero),
    (eight, zero),
    (one, zero),
    (three, zero),
    (two, zero),
    (zero, zero),
])
def test_error_division_by_zero(x, y):
    """Test that divides is not possible divides by zero"""
    with pytest.raises(ZeroDivisionError):
        x(divided_by(y))
